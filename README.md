Clarity Examples
================

Example code following up conversation with a client to illustrate points.

This code has been quickly drafted so it can be delivered to the client quickly.


Retrieving A Web Request As A Plain String
------------------------------------------
See `Assets/Scripts/WebRequestAsStringController` and the simple scene UI

Custom Unity Events With Multiple/Custom Parameters
---------------------------------------------------
See `Assets/Scripts/CustomUnityEventController` and the `CustomEventExample` object in the scene.

Threads With Unity
------------------
See `Assets/Scripts/Threads/ThreadExampleController`, the `ThreadExample` object in the scene and the console log.