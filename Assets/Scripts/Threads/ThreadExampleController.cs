﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*

    The Unity API is NOT thread safe and you can't call anything from a thread on the main unity/ui thread.

    To transfer data from threads, you need to use a coroutine to check on the thread status

*/
using System.Threading;


class SomeWork {

        public int sleep = 0;

        object hLock = new object();

        bool isDone = false;

        public bool IsDone {
            get {
                bool result;
                lock (hLock) {
                    result = isDone;
                }
                return result;
            }
            set {
                lock (hLock) {
                    isDone = value;
                }
            }
        }
        void ThreadedFunction() {
            //Do your processing here
            Thread.Sleep(sleep);

            //Put the results in a private construct, and allow the main thread to call it using
            //a Getter function that uses a lock (just in case someone tries to call it early)

        }    

        public void Run() {
            IsDone = false;
            Debug.Log("Thread is running");
            ThreadedFunction();
            Debug.Log("Thread is completing");
            IsDone = true;
        }
    }

public class ThreadExampleController : MonoBehaviour {

    int sleepTime = 4000;

    Thread myThread;
    SomeWork worker;

    bool hasCompleted = false;

	// Use this for initialization
	void Start () {
		worker = new SomeWork();
        worker.sleep = sleepTime;
        myThread = new Thread(worker.Run);
        myThread.Start();
        StartCoroutine(CheckWorker());
	}
	
	// Update is called once per frame
	void Update () {

	}

    IEnumerator CheckWorker() {
        while (!worker.IsDone) {
            Debug.Log("Waiting..."+Time.timeSinceLevelLoad);
            yield return null;
        }
        Debug.Log("Done!! "+Time.timeSinceLevelLoad);
        //Now the worker thread has data, we can go process it somewhere on the main unity thread
    }


}
