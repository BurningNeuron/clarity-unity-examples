﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class WebRequestAsStringController : MonoBehaviour {

    [Header("Settings")]
    [SerializeField] string requestURL;

    [Header("Wired Components")]
    [SerializeField] Text webRequestOutputText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DoWebRequest() {
        StartCoroutine(AsyncWebRequest());
        webRequestOutputText.text = "Retrieving from: "+requestURL;
    }

    IEnumerator AsyncWebRequest() {
        UnityWebRequest www = UnityWebRequest.Get(requestURL);
        yield return www.Send();

        if (www.isError) {
            webRequestOutputText.text = "ERROR: "+www.error;
            Debug.LogError(www.error);
            yield break;
        }

        webRequestOutputText.text = www.downloadHandler.text;

        
    }
}
