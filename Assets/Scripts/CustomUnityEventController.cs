﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Text;


//Simple custom event that emits a string
[System.Serializable]
public class CustomStringEvent : UnityEvent <string> { }

//Custom event that emits a GameObject and a Collider
[System.Serializable]
public class CustomMultiParameterEvent: UnityEvent<GameObject, Collider> { }

//Unity events can be extended up to 4 parameters..
[System.Serializable]
public class MaxedParameterEvent: UnityEvent<string, int, GameObject, bool> { }

//To work around the max limit, pass an object array and cast later....
[System.Serializable]
public class MultiParameterWithNoEnforcementEvent: UnityEvent<GameObject, object[]> { }

//Or better declare a custom class for your
public class MyPassedData {
    public string name;
    public uint health;
    public GameObject avatar;
}

[System.Serializable]
public class SpecialisedEvent : UnityEvent<MyPassedData> { }

public class CustomUnityEventController : MonoBehaviour {

    [Header("Event Emitters")]
    [SerializeField] CustomStringEvent stringEmitter;
    [SerializeField] CustomMultiParameterEvent multiParameterEmitter;
    [SerializeField] MaxedParameterEvent maxedParameterEmitter;
    [SerializeField] MultiParameterWithNoEnforcementEvent multiParamNoEnforcementEmitter;
    [SerializeField] SpecialisedEvent specialisedEmitter;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Examples() {
        stringEmitter.Invoke("Some string");
        multiParameterEmitter.Invoke(gameObject, new Collider());
        maxedParameterEmitter.Invoke("Meaning Of Life", 42, gameObject, true);

        object[] objParams = new object[5];
        objParams[0] = new object();
        objParams[1] = new StringBuilder("Initialized");
        objParams[2] = "String literal";
        objParams[3] = 3;
        objParams[4] = null;
        multiParamNoEnforcementEmitter.Invoke(gameObject, objParams);

        MyPassedData myData = new MyPassedData {
            name = "bob",
            health = 100,
            avatar = gameObject
        };
        specialisedEmitter.Invoke(myData);
    }
}
